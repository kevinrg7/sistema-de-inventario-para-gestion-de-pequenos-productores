from django.contrib import admin

from authApp.models.products import Inventory, Item
from .models.user import User
from .models.account import Account
from .models.products import Inventory
from .models.products import Item

admin.site.register(User)
admin.site.register(Account)
# Register your models here.
admin.site.register(Item)
admin.site.register(Inventory)