from django.db import models
from .user import User

class Inventory(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    quantity = models.FloatField()

class Item(models.Model):
    name =models.CharField(max_length=40)
    value = models.FloatField()
    inventory =models.ForeignKey(Inventory,on_delete=models.CASCADE)
    # Optional
    # description = models.TextField(max_length=200)


